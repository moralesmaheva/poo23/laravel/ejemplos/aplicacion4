<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\HomeController;
use App\Models\Cliente;
use Illuminate\Support\Facades\Route;

//Haciendo las rutas para los vistas con nombre para utilizarlos en el menu

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home.index');
    Route::get('/index', 'index')->name('home.index');
});

Route::controller(ClienteController::class)->group(function () {
    Route::get('/cliente', 'index')->name('cliente.index');
    Route::get('/cliente/create', 'create')->name('cliente.create');
    Route::get('/cliente/{id}', 'show')->name('cliente.show');
    Route::post('/cliente', 'store')->name('cliente.store');
    Route::get('/cliente/edit/{id}', 'edit')->name('cliente.edit');
    Route::put('/cliente/{cliente}', 'update')->name('cliente.update');
    Route::delete('/cliente/{cliente}', 'destroy')->name('cliente.destroy');
});
