<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        //tengo que leer todos los clientes y mandarlos a la vista
        //$clientes=Cliente::all();//array con los clientes

        //paginando los resultados
        $clientes = Cliente::paginate(6); //array con los clientes
        return view('cliente.index', ['clientes' => $clientes]);
    }

    public function show($id)
    {
        $cliente = Cliente::find($id);
        return view('cliente.show', ['cliente' => $cliente]);
    }

    public function create()
    {
        //nos carga un formulario para crear un nuevo cliente
        return view('cliente.create');
    }

    public function store(Request $request)
    {
        //cuando pulsas el boton de insertar me llega a este metodo
        $request->validate([
            'nombre' => 'required',
            'email' => 'required',
        ]);

        //asignacion masiva el metodo fillable en modelo cliente
        //si solo quiero algunos campos se ponen en lugar del all
        $cliente = Cliente::create($request->all());

        //sin asignacion masiva
        // $cliente = new Cliente();
        // $cliente->nombre = $request->input('nombre');
        // $cliente->email = $request->input('email');
        // $cliente->save();

        //redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', "El cliente se ha creado correctamente");
    }


    //cargar el formulario para editar
    public function edit($id)
    {
        //cargo los datos del cliente con ese id
        $cliente = Cliente::find($id);
        //mando a la vista para mostrar el formulario y editar el cliente
        return view('cliente.edit', ['cliente' => $cliente]);
    }

    //cuando pulso el boton de editar
    //recibe el id pero solo el cliente tiene el id
    public function update(Request $request, Cliente $cliente)
    {
        //validamos los datos
        $request->validate([
            'nombre' => 'required',
            'email' => 'required',
        ]);
        //actualizamos al cliente
        $cliente->update($request->all());

        //redireccionamos a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', "El cliente se ha actualizado correctamente");
    }

    //cuando pulsamos el boton de borrar
    public function destroy(Cliente $cliente){
        //eliminar el cliente
        $cliente->delete();
        //redireccionamos a la vista index
        return redirect()
        ->route('cliente.index')
        ->with('mensaje', "El cliente se ha eliminado correctamente");
    }
}
