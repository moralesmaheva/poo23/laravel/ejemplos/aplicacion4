<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    //lso campos de asignacion masiva
    //los campos del modelo sin tener q ir de uno a uno
    protected $fillable = [
        'nombre',
        'email',
    ];
}
