{{-- para cargar del layout --}}
@extends('layouts.main')

{{-- para cargar el titulo --}}
@section('titulo', 'Mostrar')

{{-- para cargar el menu --}}
@section('cabecera')
    <h1>Datos del cliente {{ $cliente->id }}</h1>
    {{-- para colocar el menu padre pero decidiendo donde --}}
    @parent
@endsection

{{-- para cargar el contenido --}}
@section('contenido')
{{-- comprobamos si existe el mensaje en la sesion de cliente creado --}}
    @if (session('mensaje'))
        <div class="row m-2">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div class="col-lg-5 m-1">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $cliente->id }}
                    </h5>
                    <p class="card-text">
                        Nombre: {{ $cliente->nombre }}
                    </p>
                    <p class="card-text">
                        Email: {{ $cliente->email }}
                    </p>
                    <div>
                        <a href="{{route('cliente.edit', $cliente->id)}}" class="btn btn-primary">Editar</a>
                        <form action="{{ route('cliente.destroy', $cliente) }} " method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
