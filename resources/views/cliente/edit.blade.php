@extends('layouts.main')

@section('titulo', 'Editar')

@section('cabecera')
    <h1> Editar cliente </h1>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div class="col-lg-5 m-1">
            <form action="{{ route('cliente.update', $cliente) }}" method="post">
                @csrf
                {{-- enviamos con put en blade porque es un update --}}
                @method('PUT')
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $cliente->nombre }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $cliente->email }}">
                </div>

                <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
    @endsection

