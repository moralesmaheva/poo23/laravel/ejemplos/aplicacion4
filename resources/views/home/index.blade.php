{{-- para cargar del layout --}}
@extends('layouts.main')

{{-- para cargar el titulo --}}
@section('titulo', 'Inicio')

{{-- para cargar el menu --}}
@section('cabecera')
    <h1>Pagina de inicio</h1>
    {{-- para colocar el menu padre pero decidiendo donde --}}
    @parent
@endsection

{{-- para cargar el contenido --}}
@section('contenido')
    <div class="row mt-3">
        <h2>Bienvenido</h2>
    </div>
@endsection
