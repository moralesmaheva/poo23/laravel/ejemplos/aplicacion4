<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo', 'Inicio')</title>
    @vite('resources/css/app.scss')
</head>

<body>
    <div class="container">
        @section('cabecera')
            @include('home._menu')
        @show


        @yield('contenido')

        {{-- el pie lo dejo fijo para que salga en todas las paginas --}}
        <div class="row mt-3">
            <div class="col-lg-6">Maheva</div>
            <div class="col-lg-6">POO24</div>
        </div>
    </div>

</body>
@vite('resources/js/app.js')

</html>
